import tkinter as tk
from tkinter import filedialog
from tkinter import ttk
from keras.models import Sequential
from keras.layers import LSTM, Dense
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from keras.models import load_model

# Create the GUI application
root = tk.Tk()
root.title("CPI Prediction")


# Prepare the dataset for LSTM input
def create_dataset(data, time_steps=1, predict=False, scaler=None):
    if len(data) < time_steps:
        raise ValueError("Please enter more future CPI values.")
    X, Y = [], []

    if predict and len(data) == time_steps:
        X.append(data[0:time_steps])
        return np.array(X)

    for i in range(len(data) - time_steps):
        X.append(data[i:i + time_steps])
        Y.append(data[i + time_steps])

    return np.array(X) if predict else (np.array(X), np.array(Y))


# Create MinMaxScaler
scaler = None
time_steps = 10
fitted_scaler = None  # To keep track of the fitted scaler

# Function to update the time_steps value
def update_time_steps():
    global time_steps
    try:
        new_time_steps = int(entry_time_steps.get())
        if new_time_steps < 1:
            raise ValueError("Time steps must be a positive integer.")
        time_steps = new_time_steps
    except ValueError as ve:
        print(ve)
        entry_time_steps.delete(0, tk.END)
        entry_time_steps.insert(0, str(time_steps))

# Function to select and load the CSV file for training
def select_csv_file():
    filename = filedialog.askopenfilename(filetypes=[("CSV files", "*.csv")])
    if filename:
        train_and_save_model(filename)


# Function to train and save the model
def train_and_save_model(filename):
    global fitted_scaler, scaler  # Use the global fitted_scaler and scaler variables

    try:
        lbl_training.config(text="Training...")
        root.update_idletasks()  # Process pending events to update the label text

        # Load and preprocess the dataset
        dataset = pd.read_csv(filename)
        cpi_values = dataset['CPI'].values.reshape(-1, 1)
        train_size = int(len(cpi_values) * 0.8)
        train_data, _ = cpi_values[:train_size], cpi_values[train_size:]

        scaler = MinMaxScaler(feature_range=(0, 1))
        train_data = scaler.fit_transform(train_data)
        fitted_scaler = scaler  # Store the fitted scaler for future use
        train_x, train_y = create_dataset(train_data, time_steps)

        # Build the LSTM model
        model = Sequential()
        model.add(LSTM(units=50, input_shape=(time_steps, 1), return_sequences=True))
        model.add(LSTM(units=50))
        model.add(Dense(units=1))
        model.compile(loss='mean_squared_error', optimizer='adam')

        # Train the LSTM model
        model.fit(train_x, train_y, epochs=50, batch_size=32)

        # Save the trained model
        save_filename = filedialog.asksaveasfilename(defaultextension=".h5")
        if save_filename:
            model.save(save_filename)
            print("Model saved successfully.")
            lbl_error.config(text="")
    except Exception as e:
        print(e, e.__cause__, e.__traceback__)
        lbl_error.config(text=str(e))
    finally:
        # Clear the "training..." message
        lbl_training.config(text="")


# Function to load the saved model and make predictions
def load_and_predict_model():
    try:
        # Load the saved model
        filename = filedialog.askopenfilename(filetypes=[("Model files", "*.h5")])
        if filename:
            model = load_model(filename)
            print("Model loaded successfully.")

            # Get user input for future data (CPI values for upcoming months)
            future_data_str = entry_future_data.get("1.0", tk.END)
            future_data_list = [float(val) for val in future_data_str.split(',') if val.strip()]
            if len(future_data_list) < time_steps:
                raise ValueError("Please enter at least {} future CPI values.".format(time_steps))
            future_data = np.array(future_data_list).reshape(-1, 1)

            # Use the fitted scaler for transformation
            global fitted_scaler
            if fitted_scaler is not None:
                future_data = fitted_scaler.transform(future_data)
            else:
                raise ValueError("Please train the model first before making predictions.")

            future_data_x = create_dataset(future_data, time_steps, predict=True)

            # Make predictions using the loaded model
            future_preds = model.predict(future_data_x)
            if fitted_scaler is not None:
                future_preds = fitted_scaler.inverse_transform(future_preds)
            else:
                raise ValueError("Please train the model first before making predictions.")
            predicted_cpi = future_preds[-1][0]
            print("Predicted CPI for the next month:", predicted_cpi)
            lbl_result.config(text="Predicted CPI for the next month: {:.2f}".format(predicted_cpi))
            lbl_error.config(text="")
    except Exception as e:
        print(e, e.__cause__, e.__traceback__)
        lbl_error.config(text=str(e))


# Create labels to display instructions
label_instruction1 = tk.Label(root, text="1. Click 'Train and Save Model' to select the CSV file for training.")
label_instruction1.grid(row=0, column=0, columnspan=2, pady=5)

label_instruction2 = tk.Label(root, text="2. Click 'Load and Predict Model' to load the model and predict.")
label_instruction2.grid(row=1, column=0, columnspan=2, pady=5)

label_instruction3 = tk.Label(root, text="3. Enter future CPI values separated by commas (e.g., 150.1,150.5,149.8,"
                                         "152.2,152.5,154.3,155.1,155.6,155.3,156.0).")
label_instruction3.grid(row=2, column=0, columnspan=2, pady=5)

# Create a button to select the CSV file for training
train_button = ttk.Button(root, text="Train and Save Model", command=select_csv_file)
train_button.grid(row=3, column=0, pady=10)

# Create a button to load the model and make predictions
predict_button = ttk.Button(root, text="Load and Predict Model", command=load_and_predict_model)
predict_button.grid(row=3, column=1, pady=10)

# Create an input field for user to input time_steps
label_time_steps = tk.Label(root, text="Enter time steps:")
label_time_steps.grid(row=4, column=0, pady=5)
entry_time_steps = ttk.Entry(root)
entry_time_steps.grid(row=4, column=1, pady=5)
entry_time_steps.insert(0, str(time_steps))  # Set the default value for time_steps
entry_time_steps.bind("<FocusOut>", lambda event: update_time_steps())

# Create an entry for user to input future data
label_future_data = tk.Label(root, text="Enter future CPI values (comma-separated):")
label_future_data.grid(row=5, column=0, columnspan=2, pady=5)
entry_future_data = tk.Text(root, height=5, width=40)
entry_future_data.grid(row=6, column=0, columnspan=2, pady=10)
default_future_data = "150.1, 150.5, 149.8, 152.2, 152.5, 154.3, 155.1, 155.6, 155.3, 156.0"
entry_future_data.insert("1.0", default_future_data)

# Create a label to display the predicted result
lbl_result = tk.Label(root, text="")
lbl_result.grid(row=7, column=0, columnspan=2)

# Create a label to display errors
lbl_error = tk.Label(root, fg="red")
lbl_error.grid(row=8, column=0, columnspan=2)

# Create a label to display "training..." message
lbl_training = tk.Label(root, text="", fg="blue")
lbl_training.grid(row=9, column=0, columnspan=2)


# Run the GUI application
root.geometry("800x500")
root.mainloop()
